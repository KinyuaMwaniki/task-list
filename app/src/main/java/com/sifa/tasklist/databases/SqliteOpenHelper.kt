package com.sifa.tasklist.databases

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.sifa.tasklist.models.ListDetailModel
import com.sifa.tasklist.models.ListModel

class SqliteOpenHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "lists.db"

        private const val TABLE_LISTS = "lists"
        private const val COLUMN_ID = "_id"
        private const val COLUMN_LIST_TITLE = "title"

        private const val TABLE_LIST_DETAILS = "lists_details"
        private const val COLUMN_LIST_DETAIL_ID = "_id"
        private const val COLUMN_LIST_DETAIL_PARENT_LIST_ID = "list_id"
        private const val COLUMN_LIST_DETAIL_TITLE = "title"
        private const val COLUMN_LIST_DETAIL_COMPLETED = "completed"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        Log.e("Create", "Create")
        val createListsTable =
            ("CREATE TABLE " + TABLE_LISTS + "(" + COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_LIST_TITLE +
                    " TEXT)")

        val createListDetailsTable =
            ("CREATE TABLE " + TABLE_LIST_DETAILS + "("
                    + COLUMN_LIST_DETAIL_ID + " INTEGER PRIMARY KEY, "
                    + COLUMN_LIST_DETAIL_PARENT_LIST_ID + " INTEGER, "
                    + COLUMN_LIST_DETAIL_TITLE + " TEXT, "
                    + COLUMN_LIST_DETAIL_COMPLETED + " INTEGER)")

        db?.execSQL(createListsTable)
        db?.execSQL(createListDetailsTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        Log.e("Update", "Update")

        db?.execSQL("DROP TABLE IF EXISTS $TABLE_LISTS")
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_LIST_DETAILS")
        onCreate(db)
    }

    fun addList(list: ListModel): Long {
        val values = ContentValues()
        values.put(COLUMN_LIST_TITLE, list.title)
        val db = this.writableDatabase
        val result = db.insert(TABLE_LISTS, null, values)
        db.close()
        return result
    }

    fun updateList(list: ListModel): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COLUMN_LIST_TITLE, list.title)
        val success =
            db.update(TABLE_LISTS, contentValues, COLUMN_ID + " = " + list.id, null)

        db.close()
        return success
    }

    fun deleteList(listItem: ListModel): Int {
        val db = this.writableDatabase
        val deleteListDetailsResult = deleteListDetailsForAList(db, listItem.id)
        if (deleteListDetailsResult >= 0) {
            val success = db.delete(TABLE_LISTS, "$COLUMN_ID = ${listItem.id}", null)
            db.close()
            return success
        }
        return -1
    }

    private fun deleteListDetailsForAList(db: SQLiteDatabase, list_id: Int): Int {
        return db.delete(TABLE_LIST_DETAILS, "$COLUMN_LIST_DETAIL_PARENT_LIST_ID = $list_id", null)
    }

    fun getAllLists(): ArrayList<ListModel> {
        val list = ArrayList<ListModel>()
        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_LISTS", null)
        while (cursor.moveToNext()) {
            val listId = cursor.getInt(cursor.getColumnIndex(COLUMN_ID))
            val listTitle = cursor.getString(cursor.getColumnIndex(COLUMN_LIST_TITLE))
            val singleList = ListModel(listId, listTitle)
            list.add(singleList)
        }
        cursor.close()
        return list
    }

    fun addListDetail(listDetailModel: ListDetailModel): Long {
        val values = ContentValues()
        values.put(COLUMN_LIST_DETAIL_PARENT_LIST_ID, listDetailModel.list_id)
        values.put(COLUMN_LIST_DETAIL_TITLE, listDetailModel.title)
        values.put(COLUMN_LIST_DETAIL_COMPLETED, listDetailModel.completed)
        val db = this.writableDatabase
        val result = db.insert(TABLE_LIST_DETAILS, null, values)
        db.close()
        return result
    }

    fun updateListDetail(listDetail: ListDetailModel): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(COLUMN_LIST_DETAIL_PARENT_LIST_ID, listDetail.list_id)
        contentValues.put(COLUMN_LIST_DETAIL_TITLE, listDetail.title)
        contentValues.put(COLUMN_LIST_DETAIL_COMPLETED, listDetail.completed)
        val success =
            db.update(
                TABLE_LIST_DETAILS,
                contentValues,
                COLUMN_LIST_DETAIL_ID + " = " + listDetail.id,
                null
            )
        db.close()
        return success
    }

    fun getListDetails(list_id: Int): ArrayList<ListDetailModel> {
        val listDetails = ArrayList<ListDetailModel>()

        val db = this.readableDatabase
        val cursor = db.rawQuery(
            "SELECT * FROM $TABLE_LIST_DETAILS WHERE $COLUMN_LIST_DETAIL_PARENT_LIST_ID = ?",
            arrayOf("$list_id")
        )
        while (cursor.moveToNext()) {
            val listDetailModel = ListDetailModel(
                cursor.getInt(cursor.getColumnIndex(COLUMN_LIST_DETAIL_ID)),
                cursor.getInt(
                    cursor.getColumnIndex(
                        COLUMN_LIST_DETAIL_ID
                    )
                ),
                cursor.getString(
                    cursor.getColumnIndex(
                        COLUMN_LIST_DETAIL_TITLE
                    )
                ),
                cursor.getInt(
                    cursor.getColumnIndex(
                        COLUMN_LIST_DETAIL_COMPLETED
                    )
                ),
            )
            listDetails.add(listDetailModel)
        }
        cursor.close()
        return listDetails
    }

    fun updateListStatus(model: ListDetailModel): Int {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        val completed: Int = if (model.completed == 1) 0 else 1

        contentValues.put(COLUMN_LIST_DETAIL_COMPLETED, completed)

        val success =
            db.update(
                TABLE_LIST_DETAILS,
                contentValues,
                COLUMN_LIST_DETAIL_ID + " = " + model.id,
                null
            )

        db.close()
        return success
    }

    fun deleteListDetail(listDetailItem: ListDetailModel): Int {
        val db = this.writableDatabase
        val success =
            db.delete(TABLE_LIST_DETAILS, "$COLUMN_LIST_DETAIL_ID = ${listDetailItem.id}", null)
        db.close()
        return success
    }


}