package com.sifa.tasklist.models

import java.io.Serializable

data class ListDetailModel(
    val id: Int,
    val list_id: Int,
    val title: String,
    val completed: Int
) : Serializable