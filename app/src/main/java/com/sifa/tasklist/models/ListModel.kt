package com.sifa.tasklist.models

import java.io.Serializable

data class ListModel(
    val id: Int,
    val title: String
) : Serializable