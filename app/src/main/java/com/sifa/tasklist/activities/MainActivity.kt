package com.sifa.tasklist.activities

import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.sifa.tasklist.adapters.ListAdapter
import com.sifa.tasklist.R
import com.sifa.tasklist.databases.SqliteOpenHelper
import com.sifa.tasklist.models.ListModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result?.resultCode == RESULT_OK) {
                getAllLists()
            } else {
                Toast.makeText(this, "Result cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar_your_lists)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Your Lists"
        }

        fabAddList.setOnClickListener {
            val intent = Intent(this, ManageListActivity::class.java)
            resultLauncher.launch(intent)
        }

        getAllLists()
    }

    private fun getAllLists() {
        val dbHandler = SqliteOpenHelper(this, null)
        val allLists = dbHandler.getAllLists()

        if (allLists.size > 0) {
            rv_lists.visibility = View.VISIBLE
            tv_no_records_available.visibility = View.GONE
            rv_lists.layoutManager = LinearLayoutManager(this)
            val listAdapter = ListAdapter(this, allLists)
            rv_lists.adapter = listAdapter

            listAdapter.setOnClickListener(object : ListAdapter.OnClickListener {
                override fun onClick(position: Int, model: ListModel) {
                    val intent = Intent(this@MainActivity, ListDetailActivity::class.java)
                    intent.putExtra(LIST_DETAILS, model)
                    startActivity(intent)
                }
            })
        } else {
            rv_lists.visibility = View.GONE
            tv_no_records_available.visibility = View.VISIBLE
        }
    }

    public fun editList(listItem: ListModel) {
        val intent = Intent(this, ManageListActivity::class.java)
        intent.putExtra(LIST_DETAILS, listItem)
        resultLauncher.launch(intent)
    }

    fun deleteList(listItem: ListModel) {
        AlertDialog.Builder(this)
            .setMessage("Confirm Delete")
            .setPositiveButton("Delete")
            { _, _ ->
                val dbHandler = SqliteOpenHelper(this, null)
                val deleteList = dbHandler.deleteList(listItem)
                if (deleteList > 0) {
                    getAllLists()
                }
            }.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }.show()
    }

    companion object {
        const val LIST_DETAILS = "list_details"
    }
}