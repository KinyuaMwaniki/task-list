package com.sifa.tasklist.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sifa.tasklist.R
import com.sifa.tasklist.databases.SqliteOpenHelper
import com.sifa.tasklist.models.ListModel
import kotlinx.android.synthetic.main.activity_add_list.*

class ManageListActivity : AppCompatActivity(), View.OnClickListener {
    private var listModel: ListModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_list)


        setSupportActionBar(toolbar_add_list)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.title = "Add List"
        }

        toolbar_add_list.setNavigationOnClickListener {
            onBackPressed()
        }

        if (intent.hasExtra(MainActivity.LIST_DETAILS)) {
            listModel =
                intent.getSerializableExtra(MainActivity.LIST_DETAILS) as ListModel
            if (actionBar != null) {
                actionBar.title = "Edit List"
            }
            et_title.setText(listModel!!.title)
        }

        btn_save.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_save -> {
                if (et_title.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Please enter a title", Toast.LENGTH_SHORT).show()
                } else {
                    if (listModel == null) {
                        val list = ListModel(0, et_title.text.toString())
                        val dbHandler = SqliteOpenHelper(this, null)
                        val addList = dbHandler.addList(list)
                        if (addList > 0) {
                            setResult(RESULT_OK, intent)
                            finish()
                        }
                    } else {
                        val list = ListModel(listModel!!.id, et_title.text.toString())
                        val dbHandler = SqliteOpenHelper(this, null)
                        val updateList = dbHandler.updateList(list)
                        if (updateList > 0) {
                            setResult(RESULT_OK, intent)
                            finish()
                        }
                    }
                }
            }
        }
    }

//    private fun addListToDatabase(list: ListModel) {
//        val dbHandler = SqliteOpenHelper(this, null)
//        dbHandler.addList(list)
//    }
}