package com.sifa.tasklist.activities

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.sifa.tasklist.R
import com.sifa.tasklist.adapters.ListDetailsAdapter
import com.sifa.tasklist.databases.SqliteOpenHelper
import com.sifa.tasklist.models.ListDetailModel
import com.sifa.tasklist.models.ListModel
import kotlinx.android.synthetic.main.activity_list_detail.*

class ListDetailActivity : AppCompatActivity() {
    var list: ListModel? = null

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result?.resultCode == RESULT_OK) {
                getListDetails(list!!.id)
            } else {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_SHORT).show()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_detail)

        if (intent.hasExtra(MainActivity.LIST_DETAILS)) {
            list = intent.getSerializableExtra(MainActivity.LIST_DETAILS) as ListModel
        }

        if (list != null) {
            setSupportActionBar(toolbar_list_detail)
            val actionBar = supportActionBar
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.title = list!!.title
            }

            toolbar_list_detail.setNavigationOnClickListener {
                onBackPressed()
            }

            fabAddListDetail.setOnClickListener {
                val intent = Intent(this, ManageListDetailActivity::class.java)
                intent.putExtra(LIST_ID, list!!.id)
                resultLauncher.launch(intent)
            }

            getListDetails(list!!.id)
        }
    }

    private fun getListDetails(list_id: Int) {
        val dbHandler = SqliteOpenHelper(this, null)
        val listDetails = dbHandler.getListDetails(list_id)

        if (listDetails.size > 0) {
            rv_list_details.visibility = View.VISIBLE
            tv_no_list_details_records_available.visibility = View.GONE
            rv_list_details.layoutManager = LinearLayoutManager(this)
            val listDetailsAdapter = ListDetailsAdapter(this, listDetails)
            rv_list_details.adapter = listDetailsAdapter

            listDetailsAdapter.setOnClickListener(object : ListDetailsAdapter.OnClickListener {
                override fun onClick(position: Int, model: ListDetailModel) {
                    updateListStatus(model)
                }
            })

        } else {
            rv_list_details.visibility = View.GONE
            tv_no_list_details_records_available.visibility = View.VISIBLE
        }
    }

    private fun updateListStatus(model: ListDetailModel) {
        val dbHandler = SqliteOpenHelper(this, null)
        dbHandler.updateListStatus(model)
        getListDetails(list!!.id)
    }

    fun editList(listDetailItem: ListDetailModel) {
        val intent = Intent(this, ManageListDetailActivity::class.java)
        intent.putExtra(LIST_DETAIL_DETAILS, listDetailItem)
        resultLauncher.launch(intent)
    }

    fun deleteListDetail(listDetailItem: ListDetailModel) {
        AlertDialog.Builder(this)
            .setMessage("Confirm Delete")
            .setPositiveButton("Delete")
            { _, _ ->
                Toast.makeText(this, "Delete List Detail", Toast.LENGTH_SHORT).show()

                val dbHandler = SqliteOpenHelper(this, null)
                val deleteList = dbHandler.deleteListDetail(listDetailItem)
                if (deleteList > 0) {
                    getListDetails(list!!.id)
                }
            }.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }.show()
    }

    companion object {
        const val LIST_ID = "list_id"
        const val LIST_DETAIL_DETAILS = "list_detail_details"
    }
}