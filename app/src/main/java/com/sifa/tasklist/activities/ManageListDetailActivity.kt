package com.sifa.tasklist.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sifa.tasklist.R
import com.sifa.tasklist.databases.SqliteOpenHelper
import com.sifa.tasklist.models.ListDetailModel
import com.sifa.tasklist.models.ListModel
import kotlinx.android.synthetic.main.activity_add_list.*
import kotlinx.android.synthetic.main.activity_add_list_detail.*

class ManageListDetailActivity : AppCompatActivity(), View.OnClickListener {
    var list_id: Int = 0
    private var listDetailModel: ListDetailModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_list_detail)

        if (intent.hasExtra(ListDetailActivity.LIST_ID)) {
            list_id = intent.getIntExtra(ListDetailActivity.LIST_ID, 0)
        }

        setSupportActionBar(toolbar_add_list_detail)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.title = "Add List Detail"
        }

        toolbar_add_list_detail.setNavigationOnClickListener {
            onBackPressed()
        }

        if (intent.hasExtra(ListDetailActivity.LIST_DETAIL_DETAILS)) {
            listDetailModel =
                intent.getSerializableExtra(ListDetailActivity.LIST_DETAIL_DETAILS) as ListDetailModel
            if (actionBar != null) {
                actionBar.title = "Edit List Detail"
            }
            et_list_detail_title.setText(listDetailModel!!.title)
        }

        btn_list_detail_save.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_list_detail_save -> {
                if (et_list_detail_title.text.isNullOrEmpty()) {
                    Toast.makeText(this, "Please enter a title", Toast.LENGTH_SHORT).show()
                } else {
                    if (listDetailModel == null) {
                        Toast.makeText(
                            this,
                            "${et_list_detail_title.text.toString()} for list ${list_id}",
                            Toast.LENGTH_SHORT
                        ).show()
                        val listDetail =
                            ListDetailModel(0, list_id, et_list_detail_title.text.toString(), 1)
                        addListDetailToDatabase(listDetail)
                        setResult(RESULT_OK, intent)
                        finish()
                    } else {
                        val listDetail = ListDetailModel(listDetailModel!!.id, listDetailModel!!.list_id, et_list_detail_title.text.toString(), listDetailModel!!.completed)
                        val dbHandler = SqliteOpenHelper(this, null)
                        val updateList = dbHandler.updateListDetail(listDetail)
                        if (updateList > 0) {
                            setResult(RESULT_OK, intent)
                            finish()
                        }
                    }
                }
            }
        }
    }

    private fun addListDetailToDatabase(listDetailModel: ListDetailModel) {
        val dbHandler = SqliteOpenHelper(this, null)
        dbHandler.addListDetail(listDetailModel)
    }
}