package com.sifa.tasklist.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.sifa.tasklist.R
import com.sifa.tasklist.activities.MainActivity
import com.sifa.tasklist.models.ListModel
import kotlinx.android.synthetic.main.list_item_row.view.*

class ListAdapter(val context: Context, val items: ArrayList<ListModel>) :
    RecyclerView.Adapter<ListAdapter.ViewHolder>() {
    private var onClickListener: OnClickListener? = null

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvPosition = view.tv_position
        val tvItem = view.tv_item
        val listMainItem = view.ll_list_item_main
    }

    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.list_item_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listItem: ListModel = items.get(position)
        holder.tvPosition.text = (position + 1).toString()
        holder.tvItem.text = listItem.title
        holder.itemView.setOnClickListener {
            if (onClickListener != null) {
                onClickListener!!.onClick(position, listItem)
            }
        }

        holder.itemView.ib_edit_list_title.setOnClickListener {
            if (context is MainActivity) {
                context.editList(listItem)
            }
        }

        holder.itemView.ib_delete_list_title.setOnClickListener {
            if (context is MainActivity) {
                context.deleteList(listItem)
            }
        }

        if (position % 2 === 0) {
            holder.listMainItem.setBackgroundColor(
                Color.parseColor("#EBEBEB")
            )
        } else {
            holder.listMainItem.setBackgroundColor(
                Color.parseColor("#ffffff")
            )
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    interface OnClickListener {
        fun onClick(position: Int, model: ListModel)
    }
}