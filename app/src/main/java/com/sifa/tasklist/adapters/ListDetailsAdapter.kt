package com.sifa.tasklist.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sifa.tasklist.R
import com.sifa.tasklist.activities.ListDetailActivity
import com.sifa.tasklist.activities.MainActivity
import com.sifa.tasklist.models.ListDetailModel
import kotlinx.android.synthetic.main.list_detail_item_row.view.*
import kotlinx.android.synthetic.main.list_item_row.view.*

class ListDetailsAdapter(val context: Context, val items: ArrayList<ListDetailModel>) :
    RecyclerView.Adapter<ListDetailsAdapter.ViewHolder>() {
    private var onClickListener: OnClickListener? = null


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvPosition = view.tv_list_detail_position
        val tvItem = view.tv_list_detail_item
        val listMainItem = view.ll_list_detail_item_main
    }

    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ListDetailsAdapter.ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.list_detail_item_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listDetailItem: ListDetailModel = items[position]
        holder.tvPosition.text = (position + 1).toString()
        holder.tvItem.text = listDetailItem.title

        holder.itemView.setOnClickListener{
            if(onClickListener != null) {
                onClickListener!!.onClick(position, listDetailItem)
            }
        }

        holder.itemView.ib_edit_list_detail_title.setOnClickListener {
            if (context is ListDetailActivity) {
                context.editList(listDetailItem)
            }
        }

        holder.itemView.ib_delete_list_detail_title.setOnClickListener {
            if (context is ListDetailActivity) {
                context.deleteListDetail(listDetailItem)
            }
        }

        if (listDetailItem.completed === 1) {
            holder.listMainItem.setBackgroundColor(
                Color.parseColor("#b2ebf2")
            )
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    interface OnClickListener {
        fun onClick(position: Int, model: ListDetailModel)
    }
}